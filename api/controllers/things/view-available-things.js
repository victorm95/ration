module.exports = {


  friendlyName: 'View available things',


  description: 'Display "Available things" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/things/available-things'
    }

  },


  fn: async function (inputs, exits) {

    const things = await Thing.find({
      owner: this.req.me.id
    });

    // Respond with view.
    return exits.success({ things });

  }


};
